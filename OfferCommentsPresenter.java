package com.wezom.rabus.backoffice.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.wezom.rabus.backoffice.App;
import com.wezom.rabus.backoffice.R;
import com.wezom.rabus.backoffice.Screens;
import com.wezom.rabus.backoffice.common.Constants;
import com.wezom.rabus.backoffice.data.network.model.Comment;
import com.wezom.rabus.backoffice.data.network.model.FullscreenPhotos;
import com.wezom.rabus.backoffice.data.network.model.Offer;
import com.wezom.rabus.backoffice.data.network.model.client.ClientInfo;
import com.wezom.rabus.backoffice.presentation.view.OfferCommentsView;
import com.wezom.rabus.backoffice.repository.OrderRepository;

import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class OfferCommentsPresenter extends BasePresenter<OfferCommentsView> {

    private static final int OFFER_COMMENTS_LIMIT = 50;

    @Inject
    OrderRepository orderRepository;
    @Inject
    Router router;

    public OfferCommentsPresenter() {
        App.INSTANCE.getApplicationComponent().inject(this);
    }

    // for testing
    public OfferCommentsPresenter(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void loadOfferComments(long orderId, int offset) {
        getViewState().showProgressDialog(true);
        unSubscribeOnDestroy(orderRepository.getOfferComments(orderId, offset, OFFER_COMMENTS_LIMIT)
                .subscribe(response -> {
                    getViewState().showProgressDialog(false);
                    if (response.isSuccess()) {
                        List<Comment> comments = response.getComments();
                        if (comments.isEmpty()) {
                            getViewState().showEmptyView(true);
                        } else {
                            getViewState().showEmptyView(false);
                            getViewState().setComments(comments);
                        }
                    } else {
                        getViewState().showMessage(Constants.Error.getTitleByCode(response.getError().getCode()));
                    }
                }, throwable -> {
                    getViewState().showProgressDialog(false);
                    getViewState().onError(throwable);
                }));
    }

    public void makeOffer(Offer offer) {
        unSubscribeOnDestroy(orderRepository.makeOffer(offer)
                .subscribe(response -> {
                    if (response.isSuccess()) {
                        getViewState().showToastMessage(R.string.order_make_offer_successful);
                    } else {
                        getViewState().showMessage(Constants.Error.getTitleByCode(response.getError().getCode()));
                    }
                }, throwable -> {
                    getViewState().onError(throwable);
                }));
    }

    public void cancelOffer(long orderId) {
        unSubscribeOnDestroy(orderRepository.cancelOffer(orderId)
                .subscribe(response -> {
                    if (response.isSuccess()) {
                        getViewState().onMessageSent();
                        getViewState().onOfferCanceled();
                    } else {
                        getViewState().showMessage(Constants.Error.getTitleByCode(response.getError().getCode()));
                    }
                }, throwable -> {
                    getViewState().onError(throwable);
                }));
    }

    public void switchToClientScreen(ClientInfo clientInfo) {
        router.navigateTo(Screens.CLIENT_SCREEN, clientInfo);
    }

    public void switchToPhotosScreen(FullscreenPhotos fullscreenPhotos) {
        router.navigateTo(Screens.PHOTOS_SCREEN, fullscreenPhotos);
    }
}
