package com.wezom.rabus.backoffice.presentation.presenter;

import com.wezom.rabus.backoffice.R;
import com.wezom.rabus.backoffice.common.Constants;
import com.wezom.rabus.backoffice.data.network.model.Comment;
import com.wezom.rabus.backoffice.data.network.model.Error;
import com.wezom.rabus.backoffice.data.network.model.Offer;
import com.wezom.rabus.backoffice.data.network.response.CancelOfferResponse;
import com.wezom.rabus.backoffice.data.network.response.MakeOfferResponse;
import com.wezom.rabus.backoffice.data.network.response.OfferCommentsResponse;
import com.wezom.rabus.backoffice.manager.ApiManager;
import com.wezom.rabus.backoffice.presentation.view.OfferCommentsView;
import com.wezom.rabus.backoffice.presentation.view.OfferCommentsView$$State;
import com.wezom.rabus.backoffice.repository.OrderRepository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OfferCommentsPresenterTest {

    private static final int SIMULATED_DELAY = 3;

    @InjectMocks
    OrderRepository orderRepository;
    @Mock
    ApiManager apiManager;
    @Mock
    OfferCommentsView$$State offerCommentsViewState;
    @Mock
    OfferCommentsView offerCommentsView;

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    private OfferCommentsPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        orderRepository = new OrderRepository(apiManager);
        presenter = new OfferCommentsPresenter(orderRepository);
        presenter.attachView(offerCommentsView);
        presenter.setViewState(offerCommentsViewState);
    }

    //region load comments
    @Test
    public void loadComments_responseSuccessful_empty() {
        OfferCommentsResponse responseSuccessful = new OfferCommentsResponse();
        responseSuccessful.setSuccess(true);
        List<Comment> comments = new ArrayList<>();
        responseSuccessful.setComments(comments);

        Observable<OfferCommentsResponse> observable = Observable.just(responseSuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        long orderId = 11;
        int offset = 0;
        int limit = 50;

        when(orderRepository.getOfferComments(orderId, offset, limit)).thenReturn(observable);

        // make request
        presenter.loadOfferComments(orderId, offset);
        verify(offerCommentsViewState).showProgressDialog(true);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).showProgressDialog(false);
        verify(offerCommentsViewState).showEmptyView(true);
    }

    @Test
    public void loadComments_responseSuccessful_notEmpty() {
        OfferCommentsResponse responseSuccessful = new OfferCommentsResponse();
        responseSuccessful.setSuccess(true);
        List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        responseSuccessful.setComments(comments);

        Observable<OfferCommentsResponse> observable = Observable.just(responseSuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        long orderId = 11;
        int offset = 0;
        int limit = 50;

        when(orderRepository.getOfferComments(orderId, offset, limit)).thenReturn(observable);

        // make request
        presenter.loadOfferComments(orderId, offset);
        verify(offerCommentsViewState).showProgressDialog(true);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).showProgressDialog(false);
        verify(offerCommentsViewState).showEmptyView(false);
        verify(offerCommentsViewState).setComments(comments);
    }

    @Test
    public void loadComments_responseUnsuccessful() {
        OfferCommentsResponse responseUnsuccessful = new OfferCommentsResponse();
        responseUnsuccessful.setSuccess(false);
        Error error = new Error(10);
        responseUnsuccessful.setError(error);
        List<Comment> comments = new ArrayList<>();
        responseUnsuccessful.setComments(comments);

        Observable<OfferCommentsResponse> observable = Observable.just(responseUnsuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        long orderId = 11;
        int offset = 0;
        int limit = 50;

        when(orderRepository.getOfferComments(orderId, offset, limit)).thenReturn(observable);

        // make request
        presenter.loadOfferComments(orderId, offset);
        verify(offerCommentsViewState).showProgressDialog(true);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).showProgressDialog(false);
        verify(offerCommentsViewState).showMessage(Constants.Error.getTitleByCode(error.getCode()));
    }

    @Test
    public void loadComments_error() {
        long orderId = 11;
        int offset = 0;
        int limit = 50;

        when(orderRepository.getOfferComments(orderId, offset, limit)).thenReturn(Observable.error(new Throwable()));

        // make request
        presenter.loadOfferComments(orderId, offset);
        verify(offerCommentsViewState).showProgressDialog(true);

        // response
        verify(offerCommentsViewState).showProgressDialog(false);
        verify(offerCommentsViewState).onError(any());
    }
    //endregion

    //region make offer
    @Test
    public void makeOffer_responseSuccessful() {
        MakeOfferResponse responseSuccessful = new MakeOfferResponse();
        responseSuccessful.setSuccess(true);

        Observable<MakeOfferResponse> observable = Observable.just(responseSuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        Offer offer = new Offer();

        when(orderRepository.makeOffer(offer)).thenReturn(observable);

        // make request
        presenter.makeOffer(offer);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).showToastMessage(R.string.order_make_offer_successful);
    }

    @Test
    public void makeOffer_responseUnsuccessful() {
        MakeOfferResponse responseUnsuccessful = new MakeOfferResponse();
        responseUnsuccessful.setSuccess(false);
        Error error = new Error(10);
        responseUnsuccessful.setError(error);

        Observable<MakeOfferResponse> observable = Observable.just(responseUnsuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        Offer offer = new Offer();

        when(orderRepository.makeOffer(offer)).thenReturn(observable);

        // make request
        presenter.makeOffer(offer);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).showMessage(Constants.Error.getTitleByCode(error.getCode()));
    }

    @Test
    public void makeOffer_Error() {
        Offer offer = new Offer();

        when(orderRepository.makeOffer(offer)).thenReturn(Observable.error(new Throwable()));

        // make request
        presenter.makeOffer(offer);

        // response
        verify(offerCommentsViewState).onError(any());
    }
    //endregion

    //region cancel offer
    @Test
    public void cancelOffer_responseSuccessful() {
        CancelOfferResponse responseSuccessful = new CancelOfferResponse();
        responseSuccessful.setSuccess(true);

        Observable<CancelOfferResponse> observable = Observable.just(responseSuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        long orderId = 11;

        when(orderRepository.cancelOffer(orderId)).thenReturn(observable);

        // make request
        presenter.cancelOffer(orderId);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).onMessageSent();
        verify(offerCommentsViewState).onOfferCanceled();
    }

    @Test
    public void cancelOffer_responseUnsuccessful() {
        CancelOfferResponse responseUnsuccessful = new CancelOfferResponse();
        responseUnsuccessful.setSuccess(false);
        Error error = new Error(10);
        responseUnsuccessful.setError(error);

        Observable<CancelOfferResponse> observable = Observable.just(responseUnsuccessful)
                .delay(SIMULATED_DELAY, TimeUnit.SECONDS);

        long orderId = 11;

        when(orderRepository.cancelOffer(orderId)).thenReturn(observable);

        // make request
        presenter.cancelOffer(orderId);

        testSchedulerRule.getTestScheduler().advanceTimeBy(SIMULATED_DELAY, TimeUnit.SECONDS);

        // response
        verify(offerCommentsViewState).showMessage(Constants.Error.getTitleByCode(error.getCode()));
    }

    @Test
    public void cancelOffer_error() {
        long orderId = 11;

        when(orderRepository.cancelOffer(orderId)).thenReturn(Observable.error(new Throwable()));

        // make request
        presenter.cancelOffer(orderId);

        // response
        verify(offerCommentsViewState).onError(any());
    }
    //endregion
}